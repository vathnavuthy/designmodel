class DesignmodelsController < ApplicationController
  before_action :set_designmodel, only: [:show, :edit, :update, :destroy]

  # GET /designmodels
  # GET /designmodels.json
  def index
    @designmodels = Designmodel.all
  end

  # GET /designmodels/1
  # GET /designmodels/1.json
  def show
  end

  # GET /designmodels/new
  def new
    @designmodel = Designmodel.new
    
  end

  # GET /designmodels/1/edit
  def edit
  end

  # POST /designmodels
  # POST /designmodels.json
  def create

    @designmodel = Designmodel.new(designmodel_params)


    respond_to do |format|
      if @designmodel.save
        format.html { redirect_to @designmodel, notice: 'Designmodel was successfully created.' }
        format.json { render :show, status: :created, location: @designmodel }
      else
        format.html { render :new }
        format.json { render json: @designmodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /designmodels/1
  # PATCH/PUT /designmodels/1.json
  def update
    respond_to do |format|
      if @designmodel.update(designmodel_params)
        format.html { redirect_to @designmodel, notice: 'Designmodel was successfully updated.' }
        format.json { render :show, status: :ok, location: @designmodel }
      else
        format.html { render :edit }
        format.json { render json: @designmodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /designmodels/1
  # DELETE /designmodels/1.json
  def destroy
    @designmodel.destroy
    respond_to do |format|
      format.html { redirect_to designmodels_url, notice: 'Designmodel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_designmodel
      @designmodel = Designmodel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def designmodel_params
      params.require(:designmodel).permit(:title, :description,tools_attributes: [:tool_name])
    end
end

json.extract! designmodel, :id, :title, :description, :created_at, :updated_at
json.url designmodel_url(designmodel, format: :json)

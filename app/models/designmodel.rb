class Designmodel < ApplicationRecord
  has_many :tools, inverse_of: :designmodel
  accepts_nested_attributes_for :tools
end

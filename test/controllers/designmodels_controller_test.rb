require 'test_helper'

class DesignmodelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @designmodel = designmodels(:one)
  end

  test "should get index" do
    get designmodels_url
    assert_response :success
  end

  test "should get new" do
    get new_designmodel_url
    assert_response :success
  end

  test "should create designmodel" do
    assert_difference('Designmodel.count') do
      post designmodels_url, params: { designmodel: { description: @designmodel.description, title: @designmodel.title } }
    end

    assert_redirected_to designmodel_url(Designmodel.last)
  end

  test "should show designmodel" do
    get designmodel_url(@designmodel)
    assert_response :success
  end

  test "should get edit" do
    get edit_designmodel_url(@designmodel)
    assert_response :success
  end

  test "should update designmodel" do
    patch designmodel_url(@designmodel), params: { designmodel: { description: @designmodel.description, title: @designmodel.title } }
    assert_redirected_to designmodel_url(@designmodel)
  end

  test "should destroy designmodel" do
    assert_difference('Designmodel.count', -1) do
      delete designmodel_url(@designmodel)
    end

    assert_redirected_to designmodels_url
  end
end

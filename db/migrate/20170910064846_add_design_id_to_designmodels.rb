class AddDesignIdToDesignmodels < ActiveRecord::Migration[5.1]
  def change
    add_column :designmodels, :design_id, :integer
  end
end
